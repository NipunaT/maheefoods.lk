<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ogani Template">

    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mahee Foods | @yield('title')</title>
    
    <link rel="shortcut icon" type="image/png" href="img/favicon.ico"/>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">
    
    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">

    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.3/dist/css/uikit.min.css" />
    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.3/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.3/dist/js/uikit-icons.min.js"></script>
    
</head>

<body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <!-- Humberger Begin -->
        <div class="humberger__menu__overlay"></div>
        <div class="humberger__menu__wrapper">
            <div class="humberger__menu__logo">
                <a href="#"><img src="img/logo2.png" alt=""></a>
            </div>
            <div class="humberger__menu__cart">
                <ul>
                    @if(Session::has("email"))
                    <li><a href="/cart"><i class="fa fa-shopping-bag"></i> <span>{{DB::table(session('cartcode'))->select("id")->count()}}</span></a></li>
                    @else
                    <li><a href="/cart"><i class="fa fa-shopping-bag"></i> <span>0</span></a></li>
                    @endif
                </ul>
                @if(session::has('sub_total'))
                <div class="header__cart__price">item: <span>Rs {{session('sub_total')}}</span></div>
                @else
                <div class="header__cart__price">item: <span>Rs 0</span></div>
                @endif
            </div>
            <div class="humberger__menu__widget">
                
                <div class="header__top__right__auth">
                    @if(Session::has("lg"))
                    <a href="/logout"><i class="fa fa-user"></i> Logout</a>
                    @else
                    <a href="/login_page"><i class="fa fa-user"></i> Login</a>
                    @endif
                </div>
            </div>
            <nav class="humberger__menu__nav mobile-menu">
                <ul>
                    <li class=@yield('homeA')><a href="/">Home</a></li>
                    <li class=@yield('shopA')><a href="/shop">Shop</a></li>
                    <li><a href="#">Pages</a>
                        <ul class="header__menu__dropdown">
                            <li><a href="./shop-details.html">Shop Details</a></li>
                            <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                            <li><a href="./checkout.html">Check Out</a></li>
                            <li><a href="./blog-details.html">Blog Details</a></li>
                        </ul>
                    </li>
                    <li><a href="./blog.html">Blog</a></li>
                    <li class=@yield('contactA')><a href="/contact">Contact</a></li>
                </ul>
            </nav>
            <div id="mobile-menu-wrap"></div>
            <div class="header__top__right__social">
                <a href="https://www.facebook.com/Maheefoodslk-112000490535292"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
            </div>
            <div class="humberger__menu__contact">
                <ul>
                    @if(Session::has("email"))
                    <li><i class="fa fa-envelope"></i>  {{session('email')}}</li>
                    @else
                    <li><i class="fa fa-id-card-o"></i><a href="/register"> Register</a></li>
                    @endif
                </ul>
            </div>
        </div>
        <!-- Humberger End -->

        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="header__top__left">
                                <ul>
                                    @if(Session::has("email"))
                                    <li><i class="fa fa-envelope"></i> {{session('email')}}</li>
                                    {{-- <a><i class="fa fa-envelope"></i>  {{session('email')}}</a> --}}
                                    @elseif(Session::has("adminemail"))
                                    <li><i class="fa fa-envelope"></i> <a href="/admin">{{session('adminemail')}}</a></li>
                                    @else
                                    <li><i class="fa fa-id-card-o"></i><a href="/register"> Register</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="header__top__right">
                                <div class="header__top__right__social">
                                    <a href="https://www.facebook.com/Maheefoodslk-112000490535292" target="_blank"><i class="fa fa-facebook"></i></a> 
                                    <a href="#"><i class="fa fa-instagram"></i></a>  
                                </div>
                                <div class="header__top__right__auth">
                                    @if(Session::has("lg"))
                                    <a href="/logout"><i class="fa fa-user"></i> Logout</a>
                                    @elseif(Session::has("admin"))
                                    <a href="/logout"><i class="fa fa-user"></i> Logout</a>
                                    @else
                                    <a href="/login_page"><i class="fa fa-user"></i> Login</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="header__logo">
                            <a href="/"><img src="{{asset('img/logo2.png')}}" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <nav class="header__menu">
                            <ul>
                                <li class=@yield('homeA')><a href="/">Home</a></li>
                                <li class=@yield('shopA')><a href="/shop">Shop</a></li>
                                <li><a href="#">Pages</a>
                                    <ul class="header__menu__dropdown">
                                        <li><a href="./shop-details.html">Shop Details</a></li>
                                        <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                                        <li><a href="./checkout.html">Check Out</a></li>
                                        <li><a href="./blog-details.html">Blog Details</a></li>
                                    </ul>
                                </li>
                                <li><a href="./blog.html">Blog</a></li>
                                <li class=@yield('contactA')><a href="/contact">contact</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3">
                        <div class="header__cart">
                            <ul>
                                @if(Session::has("email"))
                                <li><a href="/cart"><i class="fa fa-shopping-bag"></i> <span>{{DB::table(session('cartcode'))->select("id")->count()}}</span></a></li>
                                @else
                                {{-- <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li> --}}
                                <li><a href="/cart"><i class="fa fa-shopping-bag"></i> <span>0</span></a></li>
                                @endif
                            </ul>
                            @if(session::has('sub_total'))
                            <div class="header__cart__price">Total: <span>Rs {{session('sub_total')}}</span></div>
                            @else
                            <div class="header__cart__price">Total: <span>Rs 0</span></div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="humberger__open">
                    <i class="fa fa-bars"></i>
                </div>
            </div>
        </header>
        <!-- Header Section End -->

        <!-- Hero Section Begin -->
        <section class="hero hero-normal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="hero__categories">
                            <div class="hero__categories__all">
                                <i class="fa fa-bars"></i>
                                <span>All departments</span>
                            </div>
                            <ul>
                                <li><a href="#">Fresh Meat</a></li>
                                <li><a href="#">Vegetables</a></li>
                                <li><a href="#">Fruit & Nut Gifts</a></li>
                                <li><a href="#">Fresh Berries</a></li>
                                <li><a href="#">Ocean Foods</a></li>
                                <li><a href="#">Butter & Eggs</a></li>
                                <li><a href="#">Fastfood</a></li>
                                <li><a href="#">Fresh Onion</a></li>
                                <li><a href="#">Papayaya & Crisps</a></li>
                                <li><a href="#">Oatmeal</a></li>
                                <li><a href="#">Fresh Bananas</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="/serch" method="GET">
                                    {{-- <div class="hero__search__categories">
                                        All Categories
                                        <span class="arrow_carrot-down"></span>
                                    </div> --}}
                                    <input type="text" name="serch_content" placeholder="What do yo u need?">
                                    <button type="submit" class="site-btn">SEARCH</button>
                                </form>
                            </div>
                            <div class="hero__search__phone">
                                <div class="hero__search__phone__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="hero__search__phone__text">
                                    <h5>077 770 4090</h5>
                                    <span>support 24/7 time</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Hero Section End -->

    @yield('content')

    <!-- Footer Section Begin -->
    <footer class="footer spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__about__logo">
                            <a href="/"><img src="{{asset('img/logo2.png')}}" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                    <div class="footer__widget">
                        <h6>Contacts</h6>
                        <ul>
                            <li>Address: 99 kandy road yakkala</li>
                            <li>Phone: 077 770 4090</li>
                            <li>Email: info@maheefoods.lk</li>
                        </ul>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="footer__widget">
                        <h6>Join Our Newsletter Now</h6>
                        <p>Get E-mail updates about our latest shop and special offers.</p>
                        <form action="#">
                            <input type="text" placeholder="Enter your mail">
                            <button type="submit" class="site-btn">Subscribe</button>
                        </form>
                        <div class="footer__widget__social">
                            <a href="https://www.facebook.com/Maheefoodslk-112000490535292"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__copyright">
                        <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This site is developed with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://www.facebook.com/theekshana.rocksreeper" target="_blank">Nipuna Theekshana</a>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            
                            </div>
                        </div>
                    </div>
                </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('js/mixitup.min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>

   

    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
</body>

</html>