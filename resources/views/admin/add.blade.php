@extends('app')
@section('title','Upload')
@section('content')


<div class="uk-flex uk-flex-center logincard uk-width-1-1@m uk-margin">
    <div class="uk-card uk-card-default uk-card-body   uk-width-1-3@m uk-animation-slide-top-small">
            <div  class="uk-card-title uk-text-center">Add Item</div>
        

        <div class="uk-child-width-expand@m" uk-grid>

            <form action="/upload" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <!--  name -->
                    <div class="uk-margin">
                        <span class="uk-label" style="background-color: #2e3e4f">item Name</span>
                        <div class="uk-inline uk-width-1-1">
                            <span class="uk-form-icon"><i uk-icon="user"></i></span>
                            <input required class="uk-input" name="name" type="text" placeholder="First name">
                        </div>
                    </div>
                    {{-- Discription --}}
                    <div class="uk-margin">
                            <span class="uk-label" style="background-color: #2e3e4f">Discription</span>
                            <div class="uk-inline uk-width-1-1">
                                <textarea name="discription" class="uk-textarea" rows="4" placeholder="Discription"></textarea>
                            </div>
                    </div>
                    {{-- Price --}}
                    <div class="uk-margin">
                        <span class="uk-label" style="background-color: #2e3e4f">Price</span>
                        <div class="uk-inline uk-width-1-1">
                            <span class="uk-form-icon"><i uk-icon="credit-card"></i></span>
                            <input required class="uk-input" name="price" type="number" placeholder="Price">
                        </div>
                    </div>
                    {{-- Catagory --}}
                    <div class="uk-margin">
                        <span class="uk-label" style="background-color: #2e3e4f">Catagory</span>
                            <div  class="uk-inline uk-width-1-1">
                                <select name="catagory" class="uk-select">
                                    @foreach($catagories as $catagory)
                                        <option value={{$catagory->name}}>{{$catagory->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                    </div>
                    {{-- Fetured --}}
                    <div class="uk-margin">
                            <span class="uk-label" style="background-color: #2e3e4f">Fetured</span>
                            <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                                <label><input class="uk-radio" type="radio" name="fetured" value="yes"> yes</label>
                                <label><input class="uk-radio" type="radio" name="fetured" value="no" checked>No</label>
                            </div>
                    </div>
                    {{-- Image --}}
                    <div class="uk-margin">
                        <span class="uk-label" style="background-color: #2e3e4f">Image</span>
                        <div class="js-upload uk-flex uk-padding-small  uk-padding-remove-bottom" uk-form-custom>
                            <input name="image" type="file" multiple>
                            <button class="uk-button-small uk-button-primary uk-width-1-1" type="button" tabindex="-1"><i uk-icon="image" class="uk-margin-small-right"></i>Select</button>
                        </div>
                    </div>
                    {{-- upload button --}}
                    <div class="uk-margin">
                        <div class="uk-flex uk-padding-small  uk-padding-remove-bottom" >
                            <button class="uk-button-small uk-button-primary uk-width-1-1" style="background-color: #7fad39" type="submit"><i uk-icon="cloud-upload" class="uk-margin-small-right"></i>Upload</button>
                        </div>
                    </div>
            </form>

        </div>
    </div>
</div>



@endsection
