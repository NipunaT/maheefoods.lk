@extends('app')
@section('title','Login')
@section('content')


<!-- Main container -->
<div class="uk-flex uk-flex-center uk-width-1-1@m uk-margin-large-top">
    <!-- Login card -->
    <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@m uk-width-1-2@m uk-margin uk-animation-slide-top-small"
         uk-grid>
         <div class="uk-card-media-left uk-cover-container">
              <img src="../assets/back.jpg" alt="" uk-cover>
              <canvas width="200" height="400"></canvas>
         </div>
         <div>
              <div class="uk-card-body  right">
                   <form action="/login" class="uk-width-expand uk-text-center" method="get">
                        {{-- <h2>Mahee.lk</h2> --}}
                        <!-- <div class="uk-card-title uk-text-light">Login</div> -->

                        <img src="{{asset("img/logo3.png")}}" class="uk-animation-stroke" width="100"
                             uk-svg="stroke-animation: true">


                        <!-- Email -->
                        <div class="uk-margin">
                              <div class="uk-inline uk-width-1-1">
                                   <span class="uk-form-icon"><i class="fas fa-user"></i></span>
                                   <input class="uk-input" name="email"  type="email" placeholder="Your email">
                              </div>
                              @if(Session::get("email")=="email")
                                   <span class="uk-text-small uk-margin-small-left uk-align-left uk-text-danger"><i class='fa fa-times-circle'></i> User not registered</span>
                              @endif
                         </div>

                         <!-- Password -->
                         <div class="uk-margin">
                              <div class="uk-inline uk-width-1-1">
                                   <span class="uk-form-icon"><i class="fas fa-key"></i></span>
                                   <input class="uk-input" type="password" name="password" placeholder="Your password">
                              </div>
                              @if(Session::has("pw"))
                              <div><span class="uk-text-small uk-margin-small-left uk-align-left uk-text-danger"><i class='fa fa-times-circle'></i> Wrong password</span></div>
                              @endif
                    
                         @csrf
                         </div>
                        <!-- Forgot password -->
                        <p class="uk-text-right">
                             <a href="#forgotpass" uk-toggle style="text-decoration:none">Forgot password ?</a>
                        </p>

                        <!-- Login buttons -->
                        <button type="submit" class="site-btn"><i class="fa fa-user uk-margin-small-right"></i>Login</button>
                        <a href="/register" style="background-color: #2e3e4f"  class="site-btn2"> <i class="fa fa-user-plus uk-margin-small-right" ></i>Register</a>

              </div>
              </form>

              <!-- Registration type popup modal -->
             
         </div>
    </div>
</div>
</div>

<!-- POPUP SECTION -->

<!-- Forgot password -->
<div id="forgotpass" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-text-center">
         <h2 class="uk-text-large">Forgot your password ?</h2>
         <p  class="uk-text-success" >Don't warry we forget everything sometimes :)</p>
         <form action="" method="POST" class="uk-form-stacked">
              <div class="uk-margin">
                   <label class="uk-form-label" for="form-stacked-text"><i class="fa fa-envelope"></i>
                        Email</label>
                   <div class="uk-form-controls">
                        <input required class="uk-input" id="form-stacked-text" type="email" placeholder="Email">
                   </div>
              </div>

              <p class="uk-text-right">
                   <a href="#" class="site-btn3  uk-modal-close"><i class="fa fa-times-circle"></i> Cancel</a>
                   <button class="site-btn" type="submit"><i class="fa fa-paper-plane"></i> Submit</button>
              </p>
         </form>
    </div>
</div>


<!-- jQuery -->
<script src=" https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous">
    </script>

<!-- UIkit JS -->
<script src="https://cdn.jsdelivr.net/npm/uikit@3.2.6/dist/js/uikit.min.js"></script>


<!-- iziToast -->
<script src="../iziToast/js/iziToast.js"></script>

<!-- Custom js -->
<script src="../js/script.js"></script>

<script>

    //     iziToast login error
    function trige() {
         iziToast.error({
              backgroundColor: '#f85e5e',
              title: '<span class="uk-text-middle">Incorrect credentials</span>',
         })
    }

</script>

@endsection
