@extends('app')
@section('title','Home')
@section('content')


<div class="uk-flex uk-flex-center logincard uk-width-1-1@m">
    <div class="uk-card uk-card-default uk-card-body  uk-width-1-2@m uk-animation-slide-top-small">
         <div class="uk-card-title uk-text-center">Registration</div>
       

         <div class="uk-child-width-expand@m" uk-grid>

              <form action="/registerr" method="post">

                   <!-- Name -->
                   <div class="uk-child-width-1-2@m" uk-grid>

                        <!-- First name -->
                        <div>
                             <span class="uk-label" style="background-color: #2e3e4f">First Name</span>
                             <div class="uk-inline uk-width-1-1">
                                  <span class="uk-form-icon"> <i class="fa fa-user"></i></span>
                                  <input required class="uk-input" name="fname" type="text" placeholder="First name">
                             </div>
                        </div>

                        <!-- Last name -->
                        <div>
                             <span class="uk-label" style="background-color: #2e3e4f">Last Name</span>
                             <div class="uk-inline uk-width-1-1">
                                  <span class="uk-form-icon"> <i class="fa fa-user"></i></span>
                                  <input required class="uk-input" name="lname" type="text" placeholder="Last name">
                             </div>
                        </div>
                   </div>

                   <!-- Mobile number-->
                   <div class="uk-margin">
                        <span class="uk-label" style="background-color: #2e3e4f">Mobile number</span>
                        <div class="uk-inline uk-width-1-1">
                             <span class="uk-form-icon"> <i class="fa fa-mobile"></i></span>
                             <input required class="uk-input"name="mobilenumber" type="text" placeholder="07X XXXXXXX">
                        </div>
                   </div>
                   <!-- address-->
                   <div class="uk-margin">
                    <span class="uk-label" style="background-color: #2e3e4f">Address</span>
                    <div class="uk-inline uk-width-1-1">
                         <span class="uk-form-icon"> <i class="fa fa-address-card "></i></span>
                         <input required class="uk-input" name="address" type="text" placeholder="Address">
                    </div>
               </div>

                   <!-- Email-->
                   <div class="uk-margin">
                        <span class="uk-label" style="background-color: #2e3e4f">Email</span>
                        <div class="uk-inline uk-width-1-1">
                             <span class="uk-form-icon"> <i class="fa fa-envelope"></i></span>
                             <input required class="uk-input" name="email" type="email" placeholder="sample@midadiya.lk">
                        </div>
                   </div>

                   <!-- Password-->
                   <div class="uk-margin">
                        <span class="uk-label" style="background-color: #2e3e4f">Password</span>
                        <div class="uk-inline uk-width-1-1">
                             <span class="uk-form-icon"> <i class="fa fa-key"></i></span>
                             <input required class="uk-input"  name="password" type="password" placeholder="XXXXXX">
                        </div>
                   </div>

                   <!-- Confirm Password-->
                   <div class="uk-margin">
                        <span class="uk-label" style="background-color: #2e3e4f">Confirm Password</span>
                        <div class="uk-inline uk-width-1-1">
                             <span class="uk-form-icon"> <i class="fa fa-key"></i></span>
                             <input required class="uk-input" name="confirmed" type="password" placeholder="XXXXXX">
                        </div>
                   </div>

                   <div class="uk-margin uk-text-center">
                        <label><input required class="uk-checkbox uk-margin-small-right" type="checkbox">I Agreed
                             to Maheefoods.lk </label><a href="#terms" uk-toggle> Term & Conditions</a>
                   </div>
                   <div class="uk-align-right">
                    @csrf 
                        <button href=""
                             class="site-btn" style="background-color: #7fad39" type="submit">Register<i
                                  class="fa fa-arrow-circle-right uk-margin-small-left"></i></button>
                   </div>
              </form>

         </div>
    </div>
</div>




































@endsection
