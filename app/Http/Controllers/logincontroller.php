<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Admin;



class logincontroller extends Controller
{
    public function index(){

        return view('login.login');

    }
    
    public function log(){ 

        $email=Admin::select('email')->where('email',request('email'))->first();

        if($email!=null){

            $id = Admin::select('id')->where('email',request('email'))->first();
            $password = Admin::select('password')->where('id',$id->id)->first();

            if (request('password')==$password->password){ 

                    session(['admin' => 'admin']);
                    session(['adminemail' =>request('email')]);

                return redirect('/admin');

            }

        }
        else{

            //get input email from db
            $email=User::select('email')->where('email',request('email'))->first();

            //check input maill
            if($email!=null){
                //correct email

                //getting id and password for the input mail
                $id = User::select('id')->where('email',request('email'))->first();
                $password = User::select('password')->where('id',$id->id)->first();

                //validate password
                if (request('password')==$password->password){ 
                    //correct password and email
                    //redirect to loged in users

                    //add cart id to the session
                    $cartcode= User::select('cartcode')->where('email',request('email'))->first();
                    session(['cartcode' => $cartcode->cartcode]);

                    session(['lg' => 'logged']);
                    session(['email' =>request('email')]);

                    //get the sub total of the cart 
                    app('App\Http\Controllers\sub_totalcontroller')->total();

                    return redirect('/');
                }
                else{

                    //Wrong password
                    return back()->with('pw', 'password');
                    return view('login');
                }
            }
            else{
                
                //wrong email
                return back()->with('email', 'email');
                return view('login');
            }
        }
    }

    public function logout(){

        // $value = session()->pull('lg', 'logged');
        // $value = session()->pull('email', '$email');
        session()->flush();

        return redirect('/');

    }
}
