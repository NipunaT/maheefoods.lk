<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home.home');
// });
Route::get('/t', function () {
    return view('serch.serch');
});
route::get('/','homecontroller@index');

route::get('/shop','shopcontroller@index');

route::get('/item/{id}','itemcontroller@index');

route::get('/cart','cartcontroller@index');

route::get('/contact','contactcontroller@index');

//loging controller
route::get('/login','logincontroller@log');
route::get('/login_page','logincontroller@index');
route::get('/logout','logincontroller@logout');

route::get('/register','registercontroller@index');
route::post('/registerr','registercontroller@store');

//cart
route::get('/add/{id}','cartcontroller@add');
route::get('/delete_item/{id}','cartcontroller@delete');

route::get('/serch','serchcontroller@serch');

route::get('/checkout','checkoutcontroller@checkout');

route::get('/orders','ordercontroller@index');

//admin
route::get('/admin','admincontroller@index');

route::get('/itemadd','itemcontroller@add');
route::post('/upload','itemcontroller@upload');