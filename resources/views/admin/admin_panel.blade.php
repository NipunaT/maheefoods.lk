@extends('app')
@section('title','Admin')
@section('content')


<div class="uk-flex uk-flex-center logincard uk-width-1-1@m uk-margin">
    <div class="uk-card uk-card-default uk-card-body   uk-width-1-3@m uk-animation-slide-top-small">
            <div  class="uk-card-title uk-text-center">Admin panel</div>
        

        <div class="uk-child-width-expand@m" uk-grid>
            <form >

                    {{-- add item button --}}
                    <div class="uk-margin">
                        <div class="uk-flex uk-padding-small  uk-padding-remove-bottom" >
                            <a class="uk-button-small uk-button-primary uk-width-1-1 uk-text-center uk-text-bold" href="/itemadd" style="background-color: #7fad39"><i uk-icon="plus" class="uk-margin-small-right"></i>Add an item</a>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <div class="uk-flex uk-padding-small  uk-padding-remove-bottom" >
                            <a class="uk-button-small uk-button-primary uk-width-1-1 uk-text-center uk-text-bold" href="" style="background-color: #435CF6"><i uk-icon="code" class="uk-margin-small-right"></i>Update item</a>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <div class="uk-flex uk-padding-small  uk-padding-remove-bottom" >
                            <a class="uk-button-small uk-button-primary uk-width-1-1 uk-text-center uk-text-bold" href="" style="background-color: #FE0000"><i uk-icon="trash" class="uk-margin-small-right"></i>Delete item</a>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <div class="uk-flex uk-padding-small  uk-padding-remove-bottom" >
                            <a class="uk-button-small uk-button-primary uk-width-1-1 uk-text-center uk-text-bold" href="" style="background-color: #6721A2"><i uk-icon="users" class="uk-margin-small-right"></i>Register an admin user</a>
                        </div>
                    </div>
                    
                </form>

        </div>
    </div>
</div>



@endsection
