@extends('app')
@section('title','Cart')
@section('content')
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Shopping Cart</h2>
                        <div class="breadcrumb__option">
                            <a href="./index.html">Home</a>
                            <span>Shopping Cart</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th class="shoping__product">Products</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(Session::has("lg"))

                                    @foreach($cart_items as $cart_item)
                                        <tr class="{{$cart_item->cart_item_id}}">
                                            <td class="shoping__cart__item">
                                                <img src="{{asset($cart_item->img_1)}}" width="100" height="100" alt="">
                                                <h5>{{$cart_item->name}}</h5>
                                            </td>
                                            <td class="shoping__cart__price">
                                                {{$cart_item->price}}
                                            </td>
                                            <td class="shoping__cart__quantity">
                                                <div class="quantity">
                                                    <div class="pro-qty">
                                                        <input type="text" value="{{$cart_item->quantity}}">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="shoping__cart__total">
                                                {{$cart_item->quantity*$cart_item->price}}
                                            </td>
                                            <td class="shoping__cart__item__close">
                                                <a href='#' onclick="deleteitem({{$cart_item->cart_item_id}}); return false;" class="icon_close"></a>
                                            </td>
                                        </tr>
                                    @endforeach

                                @else
                                    <tr></tr>
                                @endif
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="/shop" class="primary-btn cart-btn">CONTINUE SHOPPING</a>
                        <a href="#" class="primary-btn cart-btn cart-btn-right"><span class="icon_loading"></span>
                            Upadate Cart</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__continue">
                        <div class="shoping__discount">
                            {{-- <h5>Discount Codes</h5>
                            <form action="#">
                                <input type="text" placeholder="Enter your coupon code">
                                <button type="submit" class="site-btn">APPLY COUPON</button>
                            </form> --}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__checkout">
                        <h5>Cart Total</h5>
                        @if(Session::get('sub_total')!=0)
                        <ul>
                            <li>Subtotal <span>Rs. {{Session('sub_total')}}</span></li>
                            <li>Shiping charge <span>Rs. 200</span></li>
                            <li>Total <span>Rs. {{Session('sub_total')+200}}</span></li>
                        </ul>
                        @else
                        <ul>
                            <li>Subtotal <span>Rs. 0</span></li>
                            <li>Shiping charge <span>Rs. 0</span></li>
                            <li>Total <span>Rs. 0</span></li>
                        </ul>
                        @endif
                        <a href="/checkout" class="primary-btn">PROCEED TO CHECKOUT</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shoping Cart Section End -->
    <script>
        function deleteitem(cart_item_id) {

                var item_id = cart_item_id;
                deleteing(item_id);
            
        }

        function deleteing(id) {
            

            var data = { "id": id, "_token": "{{csrf_token()}}" }
            var request = $.ajax({
                url: "/delete_item/"+id+"",
                type: "get",
                data: data,
                dataType: "json",

                success: function (res) {

                    $("."+id+"").remove();
                    
                },
            });


        }
    </script>
    
@endsection
