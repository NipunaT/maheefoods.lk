<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class checkoutcontroller extends Controller
{
   public function checkout(){

        $ordered_items=  DB::table(session('cartcode'))
                    ->select('*')
                    ->get();

        $user=DB::table('users')
            ->where('cartcode','=',session('cartcode'))
            ->get('id')->first();

        foreach($ordered_items as $ordered_item){

            DB::table('orders')->insert(
                ['itemid' => $ordered_item->itemid, 'quantity' =>$ordered_item->quantity, 'user_id' =>$user->id]
            );

        }   
        //update order nortification
        DB::table('users')->where('id', $user->id)->update(['order_available' => 1]);

        // emty the cart
        DB::table(session('cartcode'))->truncate();  

        //get the sub total of th cart 
        app('App\Http\Controllers\sub_totalcontroller')->total();            

        return redirect('/cart');

   }
}
