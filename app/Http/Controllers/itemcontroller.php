<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Storage;


class itemcontroller extends Controller
{
    public function index(){

        $item =db::table('items')->where('id','=',request('id'))->get();
        return view('item.item',compact('item'));
        
    }
    public function add(){

        $catagories =db::table('catagories')->select('name')->get();
        return view('admin.add',compact('catagories'));
        
    }
    public function upload(Request $request){

        // $validatedData = $request->validate([

        //     'name' => ['required', 'max:255'],
        //     'discription' => ['required'],
        //     'price' => ['required','integer'],
        //     'featured' => ['required'],

        // ]);

        // rename and upload the image
        $id=db::table('items')->max('id');
        $const='feature-';
        $imageid=$id+1;
        $image_name =$const.$imageid; //new image name
        $guessExtension = $request->file('image')->guessExtension(); //file extention
        $file = $request->file('image')->storeAs('featured', $image_name.'.'.$guessExtension,'public_uploads' );

        //build url for the image
        $const_url='img/featured/';
        $url=$const_url.$image_name.'.'.$guessExtension;

        // select catagory id
        $catagoryid =db::table('catagories')->where('name','=',request('catagory'))->first('id');
        
        //incert the item
        $item=new \App\Item();
        $item->name=$request->name;
        $item->discription=$request->discription;
        $item->price=$request->price;
        $item->featured=$request->fetured;
        $item->cat_id=$catagoryid->id;
        $item->img_1=$url;
        $item->save();
        return redirect('/itemadd');


    }
}
