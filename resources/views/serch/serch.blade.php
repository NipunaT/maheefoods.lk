@extends('app')
@section('title','Shop')
@section('content')

<section >
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Search results</h2>
                </div>
                <div class="featured__controls">
                </div>
            </div>
        </div>
        <div class="row featured__filter">

            @foreach ($results as $result)

                <div class="col-lg-3 col-md-4 col-sm-6 mix {{$result->cat_id}}">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg" data-setbg="{{$result->img_1}}">
                            <ul class="featured__item__pic__hover">
                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                {{-- <li><a href="#"><i class="fa fa-retweet"></i></a></li> --}}
                                <li><a href="/item/{{$result->id}}" ><i class="fa fa-shopping-cart"></i></a></li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="/item/{{$result->id}}">{{$result->name}}</a></h6>
                            <h5>Rs. {{$result->price}}</h5>
                        </div>
                    </div>
                </div>

            @endforeach
            
        </div>
    </div>
</section>
@endsection