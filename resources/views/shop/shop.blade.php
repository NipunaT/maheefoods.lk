@extends('app')
@section('title','Shop')
@section('content')
@section('shopA','active')


<section >
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                {{-- <div class="section-title">
                    <h2>Featured Product</h2>
                </div> --}}
                <div class="featured__controls">
                    <ul>
                        <li class="active" data-filter="*">All</li>
                        @foreach($Catagories as $Catagory)
                        <li data-filter=".{{$Catagory->name}}">{{$Catagory->name}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="row featured__filter">

            @foreach ($items as $item)

                <div class="col-lg-3 col-md-4 col-sm-6 mix {{$item->cat_name}}">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg" data-setbg="{{$item->img_1}}">
                            <ul class="featured__item__pic__hover">
                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                {{-- <li><a href="#"><i class="fa fa-retweet"></i></a></li> --}}
                                <li><a href="/item/{{$item->id}}" ><i class="fa fa-shopping-cart"></i></a></li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="/item/{{$item->id}}">{{$item->name}}</a></h6>
                            <h5>Rs. {{$item->price}}</h5>
                        </div>
                    </div>
                </div>

            @endforeach
            
        </div>
    </div>
</section>
@endsection