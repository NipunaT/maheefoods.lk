<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\User;
use DB;


class cartcontroller extends Controller
{
    public function index(){
 
        if(Session::has("lg")){
            //getting cart items
            $cart_items= DB::table(session('cartcode'))
                ->join('items', 'items.id', '=',session('cartcode').'.itemid')
                ->select('items.*',session('cartcode').'.quantity',session('cartcode').'.id as cart_item_id')
                ->get();


            //get the sub total of th cart 
            app('App\Http\Controllers\sub_totalcontroller')->total();

            return view('cart.cart',compact('cart_items'));
        }
        else{
            
            return view('cart.cart');
        }
         

        
    }

    public function add($id){

        // $items = \App\Item::all();
        if(Session::has("lg")){

            $cartcode= User::select('cartcode')->where('email',session('email'))->first();
            $quantity=request('quantity');

            
            DB::table($cartcode->cartcode)->insert(
                ['itemid' => $id, 'quantity' =>$quantity]
            );

            //add cart id to the session
            session(['cartcode' => $cartcode->cartcode]);

            //get the sub total of th cart 
            app('App\Http\Controllers\sub_totalcontroller')->total();

            return redirect('/shop');
        }

        else{
            return view('login.login');
        }

    }

    public function delete($id){

        DB::table(session('cartcode'))->where('id', '=', $id)->delete();

        $response = array(
            'status' => 'success',
            'msg' => 'Reson added',
        );

        //get the sub total of th cart 
        app('App\Http\Controllers\sub_totalcontroller')->total();

        return response()->json( $response );

    }
}

