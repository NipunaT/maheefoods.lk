<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class sub_totalcontroller extends Controller
{
   
    public function total(){

        //getting cart items
        $cart_items= DB::table(session('cartcode'))
        ->join('items', 'items.id', '=',session('cartcode').'.itemid')
        ->select('items.*',session('cartcode').'.quantity',session('cartcode').'.id as cart_item_id')
        ->get();

        // getting the sum of the cart items   
        $sub_total=0;

        foreach ($cart_items as $cart_item){
        
        $x = $cart_item->price*$cart_item->quantity;
        $sub_total = $x+$sub_total;
        };
        session(['sub_total' => $sub_total]);
            
    }
}
