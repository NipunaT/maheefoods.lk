<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
//for getting time
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;


class registercontroller extends Controller
{
    public function index(){



        return view('register.register');

    }
    public function store(){

        $email = User::select('email')->where('email',request('email'))->first();

        if ($email === null) {
            
            //if alredy not registered

            // assign registration data to a session
            $fname=request('fname');
            $lname=request('lname');
            $address=request('address');
            $mobilenumber=request('mobilenumber');
            $email=request('email');
            $password=request('password');

            $now = Carbon::now();
            $cartcode1 = $now->format('mYdHisu');
            $cartcode= dechex($cartcode1);
            // dd($cartcode);
             
            $User=new \App\User();
            $User->fname=$fname;
            $User->lname=$lname;
            $User->address=$address;
            $User->mobilenumber=$mobilenumber;
            $User->email=$email;
            $User->password=$password;
            $User->cartcode=$cartcode;  
            $User->save();

            
            // create cart table for user
            Schema::create($cartcode, function($table)
            {
                $table->increments('id'); //order id
                $table->integer('itemid');
                $table->integer('quantity');

            });

            //load home page
            return view('login.login');

        }
        else{

            //already registered
            return back()->with('alredyin','You are already registered!');
        }
    }
}
