<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class homecontroller extends Controller
{



    public function index(){

        $Catagories=\App\Catagory::all();
        $item= db::table('items')->where('featured','=','yes');

        $Featured_items = DB::table('catagories')
                        ->joinSub($item, 'item', function ($join) {
                         $join->on('catagories.id', '=', 'item.cat_id');
                        })
                        ->select('item.*', 'catagories.name as cat_name')->get();
        
        return view('home.home',compact('Catagories','Featured_items'));
    }
}
