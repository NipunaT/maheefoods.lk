<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class shopcontroller extends Controller
{
    public function index(){

        $Catagories=\App\Catagory::all();

        $items= db::table('items')
                ->join('catagories','catagories.id', '=', 'items.cat_id')
                ->select('items.*', 'catagories.name as cat_name')->get();
                
        return view('shop.shop',compact('items','Catagories'));
        
    }
}
